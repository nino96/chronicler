for using mysql 
1.install mysql client and server packages
2.pip install pymysql to auto-correct bindings 
2.change SQLALCHEMY_DATABASE_URI to 'mysql+pymysql://username:password@your_server/your_db_name'

NOTES:
1.pip install python-mysqldb does not seem to remove the no module named mysqldb error,so stick to pymysql solution
	2.db_create.py script from grinberg tutorial won't work once mysql database has been created since when database is already created
it tries to bring the database under exclusive version control of the repository in which it is in,but it seems database in case of mysql
already controlled and so DatabaseAlreadyControlledError is raised.
	3.linking custom css file from base template did not seem to work, so used {{ url_for('static', filename='stylesheets/style.css') }} in
	  the href attribute of <link> tag in base.html

Dependencies for newspaper:
sudo apt-get install python-dev libxml2-dev libxslt1-dev zlib1g-dev