import os



basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_TRACK_MODIFICATIONS = True
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:root@localhost/chronicler'
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir,'db_repository')

#----- for flask-login session -----
SECRET_KEY = 'blabbermouth blabbertales'
SESSION_TYPE = 'filesystem'


#------ for flask-mail ------
MAIL_SERVER = 'smtp.gmail.com'
MAIL_PORT = 465
MAIL_USE_TLS = False
MAIL_USE_SSL = True
MAIL_USERNAME = 'chronicler.rss@gmail.com'
MAIL_PASSWORD = 'readwhatyoulove'

ADMINS = ['chronicler.rss@gmail.com']



# ----- for pagination ------
ITEMS_PER_PAGE = 5

OAUTH_CREDENTIALS = {
	'facebook':{
		'id':'254449081616104',
		'secret':'f9a1ffcf9fea5b89d82d0530b61ebed3'
	},
	'google':{
		'id':'504069162693-sea6qbek02ccu0pkn7utoimpms4u4v9r.apps.googleusercontent.com',
		'secret':'l1QkLRWieuDXcV8ct5ytzBex'
	}
}

class configSchedule(object):
	JOBS = [
		{
		    'id': 'job1',
		    'func': 'app.schedules:job1',
		    'trigger': {
		        'type': 'cron',
		        'minute': 38	
		    }
		},
		{
			'id':'job_email',
			'func':'app.schedules:job_email',
			'trigger':{
				'type':'cron',
				'hour': 22

			}
		
		}
	]
#WTF_CSRF_ENABLED = True
#SECRET_KEY = 'blabbermouth blabbertales'


