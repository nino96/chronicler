from flask_wtf import Form
from wtforms import StringField,SelectField,BooleanField,PasswordField,SubmitField,ValidationError
from wtforms.validators import DataRequired,Email,Length,EqualTo,Required,Regexp,URL

from models import *
from app import db

import feedparser,httplib,urllib2

class LoginForm(Form):
	#maxmimum email length is 254 by IETF standards currently
	email = StringField('Email',validators=[Required(),Email(),Length(min=1,max=254)])
	password = PasswordField('Password',validators=[Required()])
	remember_me = BooleanField('Keep me logged in')
	submit = SubmitField('Log In')

class RegistrationForm(Form):
	email = StringField('Email',validators=[Required(),Length(min=1,max=254),Email()])
	nickname = StringField('Nickname',validators=[Required(),Length(1,64),Regexp('^[A-Za-z][A-Za-z0-9_.]*$',0,'Nickname must start with alphabet and can contain alphabets,numbers,underscores and dots')])
	password = PasswordField('Password',validators=[Required(),EqualTo('confirm',message='Passwords must match')])
	confirm = PasswordField('Confirm Password',validators=[Required()])
	submit = SubmitField('Register')
	
	#----- custom validators -----
	'''def validate_email(self,field):
		if User.query.filter_by(email=field.data).first():
			raise ValidationError('Email already registered')'''

	#in database however unique constraint not applied to nickname so change that if required 
	def validate_username(self,field):
		if User.query.filter_by(nickname=field.data).first():
			raise ValidationError('Username already in use')

class CustomFeedForm(Form):

	url = StringField('URL',validators=[Required(),Length(min=1,max=500),URL(message='Please enter a valid URL(eg: http://www.example.com)')])
	name = StringField('Website Name',validators=[Required(),Length(min=1,max=64)])
	category = SelectField('Category',coerce=int)
	#submit = SubmitField('Add Feed')

	def validate_url(self,field):

		#httplib does not allow urls in http format

		#link = httplib.HTTPConnection(field.data)
		#link.request("HEAD",'')
	
		#status 1XX is info,2XX is success , 3XX is redirect , 4XX client error and 5XX server error
		
		try:
			urllib2.urlopen(field.data)
			
			d = feedparser.parse(field.data)

			#if feed type is not known to feedparser then it returns an empty string,empty strings in python are false in boolean context,
			#so we can use the boolean test condition given below
			if not d.version:
				raise ValidationError('Please enter valid RSS/ATOM Feed')
			else:
				if Feed.query.filter_by(url=field.data).first():
					raise ValidationError('This feed already exists')
			
		except urllib2.HTTPError:
			raise ValidationError('Please enter valid reachable URL')
		except urllib2.URLError:
			raise ValidationError('Please enter valid reachable URL')	
		#except Exception:
		#	raise ValidationError('Please enter a valid URL')



