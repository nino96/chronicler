from app import db
import datetime
from flask_login import UserMixin
from app import login_manager
from werkzeug.security import generate_password_hash,check_password_hash



feed_selection = db.Table('feed_selection',
	db.Column('user_id',db.ForeignKey('users.id')),
	db.Column('link_id',db.ForeignKey('feeds.id'))
)

email_notification = db.Table('email_notification',
	db.Column('user_id',db.ForeignKey('users.id')),	
	db.Column('link_id',db.ForeignKey('feeds.id'))
)


class Feed(db.Model):
	__tablename__ = 'feeds'
	
	id = db.Column(db.Integer,primary_key=True)
	site_name = db.Column(db.String(64))
	url = db.Column(db.String(200),unique=True,index=True)
	category = db.Column(db.Integer,db.ForeignKey('categories.id'),index=True,unique=False)
	jsonData = db.Column(db.Text())

	def __repr__(self):
		return "{'url':%s,'name':%s }" % (self.url,self.site_name)

class Category(db.Model):
	__tablename__ = 'categories'

	id = db.Column(db.Integer,primary_key=True)
	name = db.Column(db.String(64))
	#the backref name and foreign key cannot be same, error occurs in that case
	feeds = db.relationship('Feed',backref='category_id')

	'''def __repr__(self):
		return "{ 'id' : %d, 'name' :%s}" % (self.id,self.name)''' 


class User(UserMixin,db.Model):
	__tablename__='users'

	id = db.Column(db.Integer,primary_key=True)
	social_id = db.Column(db.String(64),unique=True,nullable=True)         #null in case of normal login 
	nickname = db.Column(db.String(64),unique=True)                        #not unique for now 
	email = db.Column(db.String(254),index=True,unique=True)
	password_hash = db.Column(db.String(300),nullable=True)                #null in case of social login(facebook,google) 
	
	#subscribed acts like a normal query object(result of a query), can apply filter_by().first() etc
	subscribed = db.relationship('Feed',secondary= feed_selection,backref='subscribers',lazy='dynamic')
	notifs = db.relationship('Feed',secondary=email_notification,backref='email_notifiers',lazy='dynamic')
	

	first_access = db.Column(db.Integer)

	'''def __init__(self):	
		self.first_access = 0'''

	#notice lack of parentheses in datetime.datetime.utcnow(), so that datetime not set immediately but when row inserted
	#date_created = db.Column(db.DateTime,default=datetime.datetime.utcnow)    this doesn't work either

	#getter and setter methods defined using property decorator
	#advantage is that these methods are automatically called when we try to get or set the values(eg: u.password='cat' calls setter method)
	@property
	def password(self):
		raise AttributeError('password is not a readable attribute')

	@password.setter
	def password(self,password):
		self.password_hash = generate_password_hash(password,method='pbkdf2:sha256:2000',salt_length=10)

	def verify_password(self,password):
		return check_password_hash(self.password_hash,password)


	#subscription to a feed
	def subscribe(self,url):
		if not self.is_subscribed(url):
			self.subscribed.append(url)
			return self
		
	def unsubscribe(self,url):
		if self.is_subscribed(url):
			self.subscribed.remove(url)
			return self

	def is_subscribed(self,url):
		return self.subscribed.filter(feed_selection.c.link_id == url.id).count()==1 



	#email notifications
	def email_notify_subscribe(self,feed):
		if not self.is_email_notify(feed):
			self.notifs.append(feed)
			return self
	
	def email_notify_unsubscribe(self,feed):
		if self.is_email_notify(feed):
			#print("Unsubscribing "+feed.site_name)
			self.notifs.remove(feed)
			return self

	def is_email_notify(self,feed):
		return self.notifs.filter(email_notification.c.link_id == feed.id).count()==1


	def __repr__(self):
		return '<User %r>' % (self.email)

#loads a user by its primary key(creates User class instance on successful authentication,or loads already logged in user)
@login_manager.user_loader
def load_user(user_id):
	return User.query.get(int(user_id))







   	
