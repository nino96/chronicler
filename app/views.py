import feedparser
import json
import jsonpickle
import newspaper
from collections import defaultdict, OrderedDict

from flask import render_template, flash, redirect, request, url_for, jsonify
from flask_login import login_user, logout_user, login_required, current_user
from flask_paginate import Pagination
from oauth import OAuthSignIn
from forms import LoginForm, RegistrationForm, CustomFeedForm
from app import app, models, db
from morss import morss
from config import *


#url_for looks for route function of that name




@app.route('/')
def landing_page():
	return render_template('landing.html')

@app.route('/index')
@login_required  # self-explanatory,decorator provided by flask-login
def index():
	
	'''try:
		page = int(request.args.get('page', 1))
	except ValueError:
		page = 1'''

	# feeds = models.Feed.query.all() does not return new data inserted into table, IT DOES!!,but have to restart app
	# feeds = models.Feed.query.all()

	# get only those feeds that are subscribed by the user now!
	feeds = current_user.subscribed
	

	linkss = []
	for feed in feeds:
		var = models.Feed.query.get(feed.id)
		var2 = models.Category.query.get(var.category) 
		linkss.append( (var2.name,{'json':var.jsonData, 'link_id': var.id, 'link':var.url, 'name':var.site_name }) ) 
		
	s = defaultdict(list)
	
	for k, v in linkss:
		s[k].append(v)
		
	s = OrderedDict(sorted(s.items()))
	
	return render_template('root_base.html', s=s)

	'''entries = []                                          #necessary,otherwise global name entries not defined error
	for feed in feeds:
		#print(feed.url)
		d = feedparser.parse(feed.url)
		start_length = len(entries)
		for entry in d.entries:
			entries.append(entry)
			if len(entries) >= (start_length + 5):
				break
	entries_sorted = sorted(entries,key=lambda e: e.published_parsed,reverse=True)

	# even though we are using pagination , we must pass the correct set of 5 entries according to current page to the template 
	i = (page-1)*ITEMS_PER_PAGE
	entries = entries_sorted[i:i+5]
	
	# have to mention css_framework='bootstrap3' explicitly otherwise pagination.links does not render with proper styling
	pagination = Pagination(page=page,per_page=ITEMS_PER_PAGE,total=len(entries_sorted),record_name='Feeds',css_framework='bootstrap3')

	return render_template('index.html',title='Home Page',per_page=ITEMS_PER_PAGE,feeds=feeds,entries=entries,pagination=pagination)'''


'''IMPORTANT NOTE: If user had logged in using facebook/google and does not have normal account then password_hash field is NULL,
	but even if user tries to login with email of facebook/google and empty password(because facebook users also in same table, so no way to distinguish ) the validation on the form doesn't allow empty password obviously'''

@app.route('/login', methods=['GET', 'POST'])
def login():
	form = LoginForm()
	if form.validate_on_submit():
		user = models.User.query.filter_by(email=form.email.data).first()
		if user is not None and user.password_hash is not None and user.verify_password(form.password.data):
			login_user(user)
			
			if current_user.first_access is None:
				current_user.first_access = 1
				db.session.commit()
				return redirect(url_for('discover'))
				
			# if login page shown to prevent access to protected url then url to redirect to after login stored in request.args['next']
			return redirect(request.args.get('next') or url_for('index'))
		flash('Invalid username or password')
	return render_template('login.html', form=form)
	

@app.route('/logout')
@login_required
def logout():
	logout_user()
	#flash('You have been logged out')
	return redirect('/register')



@app.route('/register', methods=['GET', 'POST'])
def register():
	form = RegistrationForm()
	if form.validate_on_submit():
		user = models.User.query.filter_by(email=form.email.data).first()
		if not user:
			user = models.User(email=form.email.data, password=form.password.data, nickname=form.nickname.data)
			db.session.add(user)
			db.session.commit()
			#flash('You can now login with supplied credentials')
			return redirect(url_for('login'))
		else:
			if user.password_hash is None:
				user.password = form.password.data
				db.session.commit()
				#	flash('You can now login with supplied credentials')
				return redirect(url_for('login'))
			else:
				flash('User already exists')
	return render_template('register.html', form=form)	



# ----- social id login views -----
@app.route('/authorize/<provider>')
def oauth_authorize(provider):
	
	# if logged in already
	if not current_user.is_anonymous:
		return redirect(url_for('index'))
	oauth = OAuthSignIn.get_provider(provider)
	return oauth.authorize()



@app.route('/callback/<provider>')
def oauth_callback(provider):
	if not current_user.is_anonymous:
		return redirect(url_for('index'))

	oauth = OAuthSignIn.get_provider(provider)
	social_id, username, email = oauth.callback()
	if social_id is None:
		flash('Authentication failed.')
		return redirect(url_for('login'))

	
	# models.User since have imported only models,not models.User
	user = models.User.query.filter_by(social_id=social_id).first()
	
	if not user:
		# check if user has already made normal account then just add social_id info to that user row in user table
		user = models.User.query.filter_by(email=email).first()
		if user:
			# set(update) social_id
			user.social_id = social_id
			db.session.commit()
			flash('Your social account has been linked with your normal account,you can now login using social account as well as email 					password combination')
		else:
			user = models.User(social_id=social_id, nickname=username, email=email)
			db.session.add(user)
			db.session.commit()

	
	login_user(user, True)
	if current_user.first_access is None:
		current_user.first_access = 1
		db.session.commit()
		return redirect(url_for('discover'))
	return redirect(url_for('index'))
	


@app.route('/organize')
@login_required
def organize():

	feeds = current_user.subscribed
	links = []

	

	for feed in feeds:

		email_sub = False	
		if current_user.notifs.filter_by(url=feed.url).first() is not None:
			email_sub = True			

		cat_name = models.Category.query.get(feed.category).name
		links.append((cat_name,{'link_id':feed.id,'url':feed.url,'site_name':feed.site_name,'email_sub':email_sub}))

	#defaultdict(list) provides empty list as default for keys not initialized
	subs = defaultdict(list)

	for k,v in links:
		subs[k].append(v)

	subs = OrderedDict(sorted(subs.items()))
	return render_template('organize.html',subs=subs)
	


@app.route('/discover')
@login_required
def discover():
	cats = models.Category.query.all()
	
	return render_template('discover.html',cats=cats)



@app.route('/explore',methods=['GET'])
@login_required
def explore():
		

	if request.args.get('cat_id') is None:
		return redirect('/index')
	
	catid = request.args.get('cat_id')
	
	cat_name = models.Category.query.get(catid).name

	feeds = models.Feed.query.filter_by(category=catid)

	data = []
	for feed in feeds:
		if current_user.subscribed.filter_by(url=feed.url).first() is None:

			'''d = feedparser.parse(feed.url)
			description = d.feed.description              description not provided by all'''			

			data.append(feed)
	
	#print(data)
	return render_template('explore.html',data=data,cat_name=cat_name)



@app.route('/custom_feed',methods=['GET','POST'])
@login_required
def custom_feed():

	form = CustomFeedForm()
	
	categories = models.Category.query.order_by('name')
	form.category.choices=[(x.id,x.name) for x in categories]

	if form.validate_on_submit():
		
		url = form.url.data

		#site_name = feedparser.parse(url).feed.title		
		site_name = form.name.data

		category = form.category.data            #form returns id
		#category = models.Category.query.filter_by(name = form.category.data).first().id no need , form returns id only
		
		print(category)
		
		#create a feed with the received data
		feed = models.Feed(url = url, site_name = site_name , category = category,jsonData = None)
		print(feed)

		db.session.add(feed)
		#might have to commit before subscribing , or feed won't exist
		db.session.commit()		

		#subscribe the feed
		current_user.subscribe(feed)
		db.session.add(current_user)
		db.session.commit()

		#commit database
		flash("Feed added successfully")

	return render_template('custom_feed.html',form=form,cats=categories)





@app.route('/subscribe_email',methods=['GET','POST'])
@login_required
def subscribe_email():
	if request.args.get('email_notif_url') is None:
		return redirect('/index')

	url = request.args.get('email_notif_url')
	
	feed = models.Feed.query.filter_by(url=url).first()

	current_user.email_notify_subscribe(feed)
	db.session.add(current_user)
	db.session.commit()

	return jsonify('success')


	
@app.route('/unsubscribe_email',methods=['GET','POST'])
@login_required
def unsubscribe_email():

	if request.args.get('email_notif_url') is None:
		return redirect('/index')

	url = request.args.get('email_notif_url')
		

	feed = models.Feed.query.filter_by(url=url).first()

	current_user.email_notify_unsubscribe(feed)
	db.session.add(current_user)
	db.session.commit()

	return jsonify('success')


@app.route('/unsubscribe_feed',methods=['GET','POST'])
@login_required
def unsubsribe_feed():

	if request.args.get('url') is None:
		return redirect('/index')	

	url = request.args.get('url')
	
	feed = models.Feed.query.filter_by(url=url).first()

	current_user.unsubscribe(feed)
	#unsubscribe from email too !
	current_user.email_notify_unsubscribe(feed)	

	db.session.add(current_user)
	db.session.commit()	

	return jsonify('success')


@app.route('/store_cat',methods=['GET','POST'])
@login_required
def store_cat():
	
	if request.args.get('url') is None:
		return redirect('/index')

	url = request.args.get('url')
	emailnotif = request.args.get('notif')

	

	feed = models.Feed.query.filter_by(url=url).first()

	current_user.subscribe(feed)

	if emailnotif == 'true':
		current_user.email_notify_subscribe(feed)

	db.session.add(current_user)
	db.session.commit()

	#print(current_user.notifs.all())

	return jsonify(url)






@app.route('/fulltext', methods=['GET'])
def fulltext():
	url = request.args.get('url')
	# fullrss = morss.process(url)
	# return render_template('fullfeed.html',fullrss=fullrss.decode('utf-8'))
	return render_template('fullfeed.html', url=url)

@app.route('/fetchArticle')
def fetchArticles():
    link = request.args.get('val',None,type=unicode)
    
    
    article = newspaper.Article(link)
    article.download()
    article.parse()
    result = article.text
    image = article.top_image
    if(result is not None):
        return jsonify({'text':result, 'img':image})
       
    return jsonify(0)
   
   
   
   
   
   
   
   
   
   
