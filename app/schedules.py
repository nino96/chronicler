import logging
import time
import multiprocessing
import jsonpickle

from flask_mail import Message
from app import app,mail
from config import ADMINS
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

from app.models import *
from feedparser import parse
'''
def job():

	start = time.time()
	result = db.engine.execute("SELECT url, id FROM feeds")
	
	for row in result:
		listw = []
		c = 0
		d = parse(row['link'])
		for entry in d.entries:
			#if c>=10:
				#break
			
			listw.append(entry)
			c = c + 1

		e = jsonpickle.encode(listw)
		var = links.query.get(row['link_id'])
		var.jsonData = e
		logging.info("%s %s" % (row['link'], row['link_id']))	

	db.session.commit()
	t = time.time() - start	
	logging.info("Time taken%s" % (t) )	
'''	
def job1():
	
    	start = time.time()
	result = db.engine.execute("SELECT url, id FROM feeds")
	
	inputs = [] 	
	
	for row in result:
		inputs.append(row)

	p = multiprocessing.Pool(processes = 10)
	linkss = p.map(FetchData, inputs)
	p.close()
	p.join()

	for result in linkss:
		var = Feed.query.get(result['id'])
		if result['json'] is not None:
			var.jsonData = result['json']
			logging.info("(SQL access)Done %s" % (result['id']))
	
	t = time.time() - start
	db.session.commit()
	logging.info("Time taken%s" % (t) )

def FetchData(urls):
	    	d = parse(urls['url'])
		listw = []
		c = 0
		for entry in d.entries:
			#if c>=10:
				#break
			listw.append(entry)
			c = c + 1

		e = jsonpickle.encode(listw)
		
		logging.info("(Multiprocessor)%s %s" % (urls['url'], urls['id']))

		return ({'id':urls['id'], 'json':e})




def job_email():

	start = time.time()
	
	'''lazy loading of attribute email_notifiers requires db session,views.py automatically gets a session it seems since
		no session creation required there '''
	'''if debug=True when doing app.run() then two instances of app are created and therefore email getting sent twice
		so either disable debug or look for the solution on stackoverflow'''
	
	Session = sessionmaker()
	engine = create_engine('mysql+pymysql://root:gingerbread@localhost/chronicler')
	Session.configure(bind=engine,expire_on_commit=False)
	session = Session()
	
	users = session.query(User).all()
	print("Sending mails")

	# for all feeds
	for user in users:
		
		# user.notifs returns query object,so even if empty the following if statement will be entered
		# user.notifs.all() gives a list and so if that is empty then if statement won't be entered
		feeds = user.notifs.all()

		if feeds:	

			recipients = []
			print(user.email)
			recipients.append(user.email)
			msg = Message('Chronicler Daily Digest',sender='chronicler.rss@gmail.com',recipients = recipients)
			msg.html = '<h2>Hi,'+user.nickname+'</h2><h3>These are the today\'s articles for your subscriptions</h3><br>'

			for f in feeds:

				data = jsonpickle.decode(f.jsonData)
				entries = []
		
				count = 0

				#get latest 3 articles for each feed
				for e in data:
					entries.append(e)
					count += 1
					if count>=3:
						break		
		
		
				msg.html += '<h3>'+f.site_name+'</h3><hr><h4>Top 3 articles</h4>'
				for e in entries:
					 msg.html += '<p><a href="'+e.link+'" target="_blank">'+e.title+'</a></p>'
		
				msg.html += '<br>'
		
			
			#this statement is required to get current app context(flask has two app instances when debug mode on)
			with app.app_context():
				mail.send(msg)

	t = time.time() - start
	session.commit()
	logging.info("Time taken %s" %(t))


