import logging

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bootstrap import Bootstrap
from flask_mail import Mail
from flask_login import LoginManager
from morss import morss
from flask_apscheduler import APScheduler

from config import configSchedule



import logging

#logging.basicConfig(filename="test.log", level=logging.INFO)
#from flask.ext.bower import Bower

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)
#Bower(app)

#-------login manager-------
login_manager = LoginManager(app)
login_manager.session_protection = 'strong'
login_manager.login_view = 'login'       #endpoint for login page

bootstrap = Bootstrap(app)
mail = Mail(app)

def fulltext(url):
	return morss.process(url)

app.jinja_env.globals.update(fulltext=fulltext)

#Scheduler

app.config.from_object(configSchedule())
scheduler = APScheduler()
scheduler.init_app(app)
logging.basicConfig()

from app import views,models
