var article_link;
var article_title;
var article_summary;
$('#container1').on('click', '.card', function(event) {
	article_link = $(this).data('article_link');
	article_title = $(this).data('article_title');
	article_summary = $(this).data('article_summary');


	//setup the share links
	//$('.g-plus').attr("data-href",article_link);
	$('#g-plus').attr("href","https://plus.google.com/share?url="+article_link);
	$('#email-share').attr("href","mailto:?subject=I wanted you to see this site&amp;body=Check out this article "+article_title+"%0D0A"+article_link);
	$('#fb-share-button').attr("href","https://www.facebook.com/sharer/sharer.php?u="+article_link);
	//gapi.plus.render("g-plus-container",{"href":article_link});


	
	$('#article_title').text(article_title);
	$('#article_summary').text(article_summary);
	$('#article_loader').show();
	//$('#article_loader').show();
	
	$('.bd-example-modal-lg').modal('show');
	console.log("Here");
	
	$.getJSON('/fetchArticle', {
		val : article_link
	}, function(data) {
		$('#article_loader').hide();
		$('#article_content').text(data.text);
		$('#article_image').attr('src',data.img);
		console.log(data);
	});
});

$('.bd-example-modal-lg').on('hide.bs.modal', function(e) {
	var modal = $(this);
	console.log("Here");

	modal.find('#article_content').text(" ");
	modal.find('#article_image').attr('src', " ");
});
