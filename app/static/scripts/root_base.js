function displaySelected(json, name) {
	var c = json;
	console.log(c);

	var q = document.getElementsByClassName("body");
	q[0].scrollTop = 0;

	var e = document.getElementById("titl");
	e.innerHTML = " ";
	e.appendChild(document.createTextNode(name));

	var f = document.getElementById("container1");
	f.innerHTML = " ";

	var i = 0;
	while (i < c.length) {

		var row = document.createElement("div");
		row.className = 'row';

		for ( j = 0; j < 4; j++) {
			if (i >= c.length)
				break;
			//console.log(c[i].title);

			//Column
			var col = document.createElement("div");
			col.className = 'col-md-3';
			row.appendChild(col);

			//Column->Card
			var card_entry = document.createElement("div");
			card_entry.className = 'card';

			card_entry.setAttribute('data-article_link', c[i].link);
			col.appendChild(card_entry);

			//Column->Card->img
			var img_holder = document.createElement("div");
			card_entry.appendChild(img_holder);

			var img = document.createElement("img");
			img.className = 'card-img-top';

			var image;
			for ( k = 0; k < c[i].links.length; k++) {
				if (c[i].links[k].type.match('image.*')) {
					console.log(c[i].links[k].href);
					image = c[i].links[k].href;
				}
			}
					
			
			if (image != null) {
				//console.log(images[0].src);
				img.src = image;
				img.style.height = '100%';
				img.style.width = '100%';
			} else {
				img_holder.style.display = 'none';
			}
			img_holder.appendChild(img);

			//Column->Card->img
			//            ->card block
			var card_block = document.createElement("div");
			card_block.className = 'card-block';
			card_entry.appendChild(card_block);

			//Column->Card->img
			//            ->card block->h6(Card title)
			//                        ->p(Card text)
			var t = c[i].title;
			card_entry.setAttribute('data-article_title', he.decode(c[i].title));
			if (c[i].title.length > 85) {
				t = c[i].title.substr(0, 85);
				t = t + '...';
			}

			var title = document.createElement("h6");
			title.className = 'card-title';
			title.appendChild(document.createTextNode(he.decode(t)));
			card_block.appendChild(title);

			var supp_text = document.createElement("p");
			supp_text.className = 'card-text';
	
			

			var sanitized_summ = c[i].summary.replace(/<\/?[^>]+>/g, '');
			supp_text.appendChild(document.createTextNode(he.decode(sanitized_summ)));
	
			
			if (sanitized_summ.length > 600) {
				sanitized_summ = t + '...';
			}
			card_entry.setAttribute('data-article_summary', he.decode(sanitized_summ));
			//Column->Card->img
			//            ->card block->h6(Card title)
			//                        ->p(Card text)
			//            ->card footer
			var card_footer = document.createElement("div");
			card_footer.className = 'card-block text-muted';
			card_entry.appendChild(card_footer);
			card_footer.appendChild(document.createTextNode(name + ' | ' + c[i].published.substr(0, 22)));

			i++;
		}
		f.appendChild(row);
	}

}

function select(json, name) {
	var c = json;

	var image;
	for ( k = 0; k < c[0].links.length; k++) {
		if (c[0].links[k].type.match('image.*')) {
			console.log(c[0].links[k].href);
			image = c[0].links[k].href;
		}
	}

	if (image != null) {
		displayLatest(4, 'col-md-3', json, name, '');
	} else {
		displayLatest(2, 'col-md-6', json, name, 'no-pic');
	}
}

function displayLatest(loop, layout, json, name, class_append) {
	var c = json;
	console.log(c[0].title);

	var f = document.getElementById("container1");

	//var x=['#FFD740', '#BCAAA4','#4CAF50','#90A4AE'];
	//var x=['#f8b195', '#f67280','#c06c84','#6c5b78'];
	//var x=['#CCCCCC', '#003366','#FFFFFF','#003366'];

	var t = document.createElement("div");
	t.className = "name";
	t.appendChild(document.createTextNode(name));
	f.appendChild(t);
	var i = 0;
	while (i < 4) {
		var row = document.createElement("div");
		row.className = 'row';

		for ( j = 0; j < loop; j++) {

			if (i >= c.length)
				break;
			//console.log(c[i].title);

			//Column
			var col = document.createElement("div");
			col.className = layout;
			row.appendChild(col);

			//Column->Card
			var card_entry = document.createElement("div");
			card_entry.className = 'card ' + class_append;

			if (class_append == 'no-pic') {
				//console.log(x[0]);
				//card_entry.style.background= x[i];
			}

			card_entry.setAttribute('data-article_link', c[i].link);
			col.appendChild(card_entry);

			//Column->Card->img
			var img_holder = document.createElement("div");
			card_entry.appendChild(img_holder);

			var img = document.createElement("img");
			img.className = 'card-img-top';

			var image;
			for ( k = 0; k < c[i].links.length; k++) {
				if (c[i].links[k].type.match('image.*')) {
					console.log(c[i].links[k].href);
					image = c[i].links[k].href;
				}
			}
			if (image != null) {
				//console.log(images[0].src);
				img.src = image;
				img.style.height = '100%';
				img.style.width = '100%';
			} else {
				img_holder.style.display = 'none';
			}
			img_holder.appendChild(img);

			//Column->Card->img
			//            ->card block
			var card_block = document.createElement("div");
			card_block.className = 'card-block';
			card_entry.appendChild(card_block);

			//Column->Card->img
			//            ->card block->h6(Card title)
			//                        ->p(Card text)
			var t = c[i].title;
			card_entry.setAttribute('data-article_title', c[i].title);
			if (c[i].title.length > 85) {
				t = c[i].title.substr(0, 85);
				t = t + '...';
			}

			var title = document.createElement("h6");
			title.className = 'card-title ' + class_append;
			title.appendChild(document.createTextNode(t));
			card_block.appendChild(title);

			var supp_text = document.createElement("p");
			supp_text.className = 'card-text';

			var sanitized_summ = c[i].summary.replace(/<\/?[^>]+>/g, '');
			supp_text.appendChild(document.createTextNode(he.decode(sanitized_summ)));

			if (sanitized_summ.length > 600) {
				sanitized_summ = t + '...';
			}
			card_entry.setAttribute('data-article_summary', he.decode(sanitized_summ));
			//Column->Card->img
			//            ->card block->h6(Card title)
			//                        ->p(Card text)
			//            ->card footer
			var card_footer = document.createElement("div");
			card_footer.className = 'card-block text-muted';
			card_entry.appendChild(card_footer);
			card_footer.appendChild(document.createTextNode(name + ' | ' + c[i].published.substr(0, 22)));

			i++;
		}
		f.appendChild(row);
	}

	var b = document.createElement("br");
	f.appendChild(b);

}

